package org.lanit.modelsJson;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestJson{

	@JsonProperty("address")
	private String address;

	@JsonProperty("phone")
	private String phone;

	@JsonProperty("debitBalance")
	private int debitBalance;

	@JsonProperty("guid")
	private String guid;

	@JsonProperty("registered")
	private String registered;

	@JsonProperty("id")
	private String id;

	@JsonProperty("isActive")
	private boolean isActive;

	@JsonProperty("friends")
	private List<FriendsItem> friends;

	@JsonProperty("creditBalance")
	private int creditBalance;

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setDebitBalance(int debitBalance){
		this.debitBalance = debitBalance;
	}

	public int getDebitBalance(){
		return debitBalance;
	}

	public void setGuid(String guid){
		this.guid = guid;
	}

	public String getGuid(){
		return guid;
	}

	public void setRegistered(String registered){
		this.registered = registered;
	}

	public String getRegistered(){
		return registered;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setIsActive(boolean isActive){
		this.isActive = isActive;
	}

	public boolean isIsActive(){
		return isActive;
	}

	public void setFriends(List<FriendsItem> friends){
		this.friends = friends;
	}

	public List<FriendsItem> getFriends(){
		return friends;
	}

	public void setCreditBalance(int creditBalance){
		this.creditBalance = creditBalance;
	}

	public int getCreditBalance(){
		return creditBalance;
	}
}