package org.lanit.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.RandomStringUtils;
import org.lanit.modelsJson.RequestJson;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.tinylog.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;


@Controller
public class JSONController {

    @GetMapping(value = "json")
    public Object getResponse (@RequestParam(value = "id") String id) throws IOException {
//      Setting start time
        long startTime = System.currentTimeMillis();
//      Reading template file into string
        String templateResponse = Files.readString(Paths.get("src\\main\\resources\\files\\json\\getResponse.json"), StandardCharsets.UTF_8);
//      Generate random values
        String randomBalance = RandomStringUtils.randomNumeric(4);
        String randomAddress = RandomStringUtils.randomAlphanumeric(50);
        UUID uuid = UUID.randomUUID();
//      Setting time in format
        LocalDateTime dt = LocalDateTime.now();
        DateTimeFormatter dtFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
        String formattedDT = dtFormatter.format(dt);
//      Preparing string
        String responseBody = String.format(templateResponse, id, uuid, randomBalance, randomAddress, formattedDT);
//      Logging response and processing time
        Logger.info(String.format("Заглушка отработала за %s мс. ID клиента - %s. UUID ответа - %s.", System.currentTimeMillis() - startTime, id, uuid));
//      Returning response with parameterized body and headers
        return ResponseEntity.ok().header("content-type", "application/json").body(responseBody);
    }

    @PostMapping(value = "json")
    public Object postResponse (@RequestBody String requestBody) throws IOException {
//      Setting start time
        long startTime = System.currentTimeMillis();
//      Reading file into string
        String templateResponse = Files.readString(Paths.get("src\\main\\resources\\files\\json\\postResponse.json"), StandardCharsets.UTF_8);
//      Generate random values
        UUID uuid = UUID.randomUUID();
//      Setting time in format
        LocalDateTime dt = LocalDateTime.now();
        DateTimeFormatter dtFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
        String lastActiveDT = dtFormatter.format(dt);
        try {
//          Converting json to classes
            ObjectMapper objectMapper = new ObjectMapper();
            RequestJson requestJson = objectMapper.readValue(requestBody, RequestJson.class);
//          Parsing json
            String id = requestJson.getId();
            int balance = requestJson.getDebitBalance() + requestJson.getCreditBalance();
            String registeredDT = requestJson.getRegistered();
            int numberOfFriends = requestJson.getFriends().size();
//          Preparing string
            String responseBody = String.format(templateResponse, id, uuid, balance, numberOfFriends, registeredDT, lastActiveDT);
//          Logging response and processing time
            Logger.info(String.format("Заглушка отработала за %s мс. ID клиента - %s. UUID ответа - %s.", System.currentTimeMillis() - startTime, id, uuid));
//          Returning response with parameterized body and headers
            return ResponseEntity.ok().header("content-type", "application/json").body(responseBody);

        }catch (Exception e){
//          Logging error message and request body if json parsing failed
            Logger.error(String.format("%s\n%s", e.getMessage(), requestBody));
//          Returning response with status 400 and request body
            return ResponseEntity.badRequest().header("content-type", "application/json").body(String.format("{\"message\": \"Передана невалидная json\", \"request\": \"%s\"}", requestBody));
        }
    }
}
