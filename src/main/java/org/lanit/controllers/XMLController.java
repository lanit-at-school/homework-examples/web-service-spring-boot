package org.lanit.controllers;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import org.tinylog.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

@Controller
public class XMLController {

    @PostMapping(value = "xml")
    public Object postResponse(@RequestBody String requestBody) throws IOException, ParserConfigurationException, SAXException {
//      Setting start time
        long startTime = System.currentTimeMillis();
//      Reading template files into string
        String templateResponseClient = Files.readString(Paths.get("src\\main\\resources\\files\\xml\\postResponseClient.xml"), StandardCharsets.UTF_8);
        String templateResponseProduct = Files.readString(Paths.get("src\\main\\resources\\files\\xml\\postResponseProduct.xml"), StandardCharsets.UTF_8);
//      Generate random values
        UUID responseUUID = UUID.randomUUID();
//      Create object Document for parse xml request
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new InputSource(new StringReader(requestBody)));
//      Parsing root tag
        Node root = document.getElementsByTagName("root").item(0);
        Element rootEl = (Element) root;
//      Taking the value inside the tag "uuid"
        String requestUUID = rootEl.getElementsByTagName("uuid").item(0).getTextContent();

        if (rootEl.getElementsByTagName("clientInfo").getLength() != 0) {
//          Taking the value inside the tag
            Node clientInfo = rootEl.getElementsByTagName("clientInfo").item(0);
            Element clientInfoEl = (Element) clientInfo;
            String clientCode = clientInfoEl.getElementsByTagName("clientCode").item(0).getTextContent();
//          Setting time in format
            LocalDateTime dt = LocalDateTime.now();
            DateTimeFormatter dtFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String endDate = dtFormatter.format(dt);
//          Generate random values
            String randomCardNumber = RandomStringUtils.randomNumeric(16);
            String randomBalance = RandomStringUtils.randomNumeric(4);
//          Preparing string
            String responseBody = String.format(templateResponseClient, clientCode, randomCardNumber, randomBalance, endDate, responseUUID);
//          Logging response and processing time
            Logger.info(String.format("Заглушка отработала за %s мс. ClientCode - %s. UUID запроса - %s. UUID ответа - %s.", System.currentTimeMillis() - startTime, clientCode, requestUUID, responseUUID));
//          Returning response with parameterized body and headers
            return ResponseEntity.ok().header("content-type", "text/xml").body(responseBody);

        } else if (rootEl.getElementsByTagName("productInfo").getLength() != 0) {
//          Taking the value inside the tag
            String productCode = rootEl.getElementsByTagName("productCode").item(0).getTextContent();
//          Generate random values
            String randomCount = RandomStringUtils.randomNumeric(2);
            String randomPrice = RandomStringUtils.randomNumeric(3);
//          Preparing string
            String responseBody = String.format(templateResponseProduct, productCode, randomCount, randomPrice, responseUUID);
//          Logging response and processing time
            Logger.info(String.format("Заглушка отработала за %s мс. ProductCode - %s. UUID запроса - %s. UUID ответа - %s.", System.currentTimeMillis() - startTime, productCode, requestUUID, responseUUID));
//          Returning response with parameterized body and headers
            return ResponseEntity.ok().header("content-type", "text/xml").body(responseBody);
        }
//      Logging request body if tag is not found
        Logger.error(requestBody);
//      Returning response with status 400 and request body
        return ResponseEntity.badRequest().body(String.format("Не найден тип запроса - \n%s", requestBody));
    }
}
